package android.databinding;

public class DataBinderMapperImpl extends MergedDataBinderMapper {
  DataBinderMapperImpl() {
    addMapper(new com.player.music.DataBinderMapperImpl());
    addMapper(new V1CompatDataBinderMapperImpl());
  }
}
