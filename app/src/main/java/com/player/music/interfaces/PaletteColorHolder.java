package com.player.music.interfaces;

import android.support.annotation.ColorInt;

public interface PaletteColorHolder {

    @ColorInt
    int getPaletteColor();
}
