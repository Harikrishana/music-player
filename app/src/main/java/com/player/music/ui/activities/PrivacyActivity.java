package com.player.music.ui.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

import com.player.music.R;
import com.player.music.util.Util;

public class PrivacyActivity extends AppCompatActivity {


    Activity activity;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        activity = PrivacyActivity.this;

        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);

        loadURL();

    }

    public void loadURL() {
        if (Util.isNetworkAvailable(this)) {

            webView.loadUrl("https://www.freeprivacypolicy.com/privacy/view/a5902d33e25c8bed2e3fcae339e13f5f");

        } else {

            Dialog();
        }
    }

    public void Dialog() {
        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(activity);
        }
        builder.setTitle("No Internet Connection!")
                .setMessage("Please Check Internet Connection.")
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        finish();
                        dialog.cancel();

                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        activity.finish();

    }
}
